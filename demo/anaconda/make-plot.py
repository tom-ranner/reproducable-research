from matplotlib import pyplot as plt
import numpy as np

data = np.genfromtxt('output/darts.out', delimiter=',')

plt.plot(data[:, 1], data[:, 2])
plt.savefig('figs/darts.pdf')
