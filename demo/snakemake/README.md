To generate the paper simply run

	$ snakemake -j1 --use-conda --use-singulaity

# Dependencies

This code seems to only depend on `snakemake` (version 5.24.1). This can be installed via anaconda (see `environment.yml` file).
