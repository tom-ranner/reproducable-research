#include <fstream>
#include <cassert>
#include <random>

struct double_generator {
  double_generator(int seed=123)
    : generator(seed),
      dis(0.0, 1.0) {}

  std::mt19937 generator;
  std::uniform_real_distribution<double> dis;

  double next() {
    return dis(generator);
  }
};

bool throw_dart(double x, double y) {
  return x*x + y*y < 1;
}

double pi_estimate(unsigned hits, unsigned thrown) {
  return 4.0 * static_cast<double>(hits) / static_cast<double>(thrown);
}


int main() {
  double_generator rand;

  unsigned total = 1000;
  unsigned hits = 0;

  std::ofstream f;
  f.open("output/darts.out");
  assert(f.is_open());
  f.precision(17);

  for(unsigned dart = 0; dart < total; ++dart) {
    double x = rand.next();
    double y = rand.next();
    bool hit = throw_dart(x, y);
    hits += static_cast<unsigned>(hit);
    f << hits << "," << dart+1 << ","
      << pi_estimate(hits, dart+1) << "\n";
  }

  f.close();

  return 0;
}
