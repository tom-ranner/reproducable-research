from matplotlib import pyplot as plt
import numpy as np

data = np.genfromtxt('output/darts.out', delimiter=',')

error = [abs(d - np.pi) for d in data[:, 2]]

plt.semilogy(data[:, 1], error)
plt.savefig('figs/darts-log.pdf')
